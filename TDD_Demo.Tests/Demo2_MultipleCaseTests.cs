﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDD_Demo.BLL;

namespace TDD_Demo.Tests
{
    class Demo2_MultipleCaseTests
    {
        [TestCase(10, 10, 20)]
        [TestCase(1, 10, 11)]
        [TestCase(10, -10, 0)]
        [TestCase(10, -5, 0)] //This should fail
        [Category("add")]
        public void ShouldAddTwoNumbersMultiple(int firstNum, int secondNum, int expectedResult)
        {
            var Cal = new Calculator();
            var result = Cal.Add(firstNum, secondNum);

            Assert.That(result, Is.EqualTo(expectedResult));
        }
    }
}
