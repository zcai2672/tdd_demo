﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TDD_Demo.BLL;
using System.Collections;

namespace TDD_Demo.Tests
{
    [TestFixture]
    public class Demo5_IntermittenBugTests
    {
        IntermittenBug obj;

        [Test]
        public void ShouldSometimesFail()
        {
            obj.unstableFunction();
        }

        [Test]
        [Repeat(100000)]
        public void ShouldMostlikelyFail()
        {
            obj.unstableFunction();
            
        }

        [SetUp]
        public void SetupBeforeTest()
        {
             obj = new IntermittenBug();
             Console.WriteLine("Before {0}", TestContext.CurrentContext.Test.Name);
        }

        [TearDown]
        public void TearDownAfterEachTest()
        {
            obj = null;
            Console.WriteLine("After {0}", TestContext.CurrentContext.Test.Name);
        }


        [TestFixtureSetUp]
        public void SetupBeforeTestFixture()
        {
            Console.WriteLine("Run before Test Fixture");
        }

        [TestFixtureTearDown]
        public void TearDownAfterTestFixture()
        {
            obj = null;
            Console.WriteLine("Run after Test Fixture");
        }
    }
}
