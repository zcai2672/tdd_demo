﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TDD_Demo.BLL;
using System.Collections;

namespace TDD_Demo.Tests
{
    [TestFixture]
    public class Demo4_MaxTimeTests
    {
        [Test]
        [MaxTime(1000)]
        public void ShouldTimeout()
        {
            var Cal = new Calculator();
            var result = Cal.LongAndComplexFunction();

        }

        [Test]
        [MaxTime(3000)]
        public void ShouldPass()
        {
            var Cal = new Calculator();
            var result = Cal.LongAndComplexFunction();

            Assert.That(result, Is.TypeOf<int>());
        }
    }
}
