﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDD_Demo.BLL;

namespace TDD_Demo.Tests
{
    [SetUpFixture]
    class SetupFixtureForSpecificNameSpace
    {
        [SetUp]
        public void RunBeforeNamespace()
        {
            Console.WriteLine("Run before namespace");
        }

        [TearDown]
        public void RunAfterNamespace()
        {
            Console.WriteLine("Run after namespace");
        }
    }
}
