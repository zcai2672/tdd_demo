﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TDD_Demo.BLL;

namespace TDD_Demo.Tests
{
    class Demo6_CalculatorDivideTests
    {
   /*     [Test]
        public void ShouldDivide(
            [Values(10, 5, 20)]int numerator, 
             [Values(2, 3, 0)]int denominator
            )
        {
            var cal = new Calculator();
            cal.Divide(numerator, denominator);
            // checking if there's no exceptions
        }
        */

        [Test]
        public void ShouldDivideSimple()
        {
            var cal = new Calculator();
            decimal? result = cal.Divide(8, 2);
            Assert.AreEqual(4, result);
            // checking if there's no exceptions
        }

        [Test]
        public void ShouldNotCrash()
        {
            var cal = new Calculator();
            decimal? result = cal.Divide(8, 0);
         //   Assert.AreEqual(4, result);
        }

        [Test]
        public void ShouldReturnNullWhenDivideZero()
        {
            var cal = new Calculator();
            decimal? result = cal.Divide(8, 0);
            Assert.AreEqual(null, result);

        }
       
    }
}
