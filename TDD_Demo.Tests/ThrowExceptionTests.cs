﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TDD_Demo.BLL;
using System.Collections;

namespace TDD_Demo.Tests
{
    [TestFixture]
    public class ThrowExceptionTests
    {
        [Test]
        public void ShouldThrowException()
        {
            var Cal = new ThrowException();
            Assert.That(() => Cal.ToBoolean("something"), Throws.Exception);
        }

        [Test]
        public void ShouldThrowAugumentNullException()
        {
            var Cal = new ThrowException();
            Assert.That(() => Cal.ToBoolean("something"), Throws.TypeOf<ArgumentNullException>());
        }
    }
}
