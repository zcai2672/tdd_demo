﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TDD_Demo.BLL;
using System.Collections;

namespace TDD_Demo.Tests
{
    [TestFixture]
    public class Demo3_ReuseDataTests
    {

        [TestCaseSource(typeof(ExampleTestCaseSource))]
        [Category("add")]
        public void ShouldAddTwoNumbersSource(int firstNum, int secondNum, int expectedResult)
        {
            var Cal = new Calculator();
            var result = Cal.Add(firstNum, secondNum);

            Assert.That(result, Is.EqualTo(expectedResult));
        }
    
    }


    public class ExampleTestCaseSource : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return new[] { 10, 10, 20 };
            yield return new[] { 10, -10, 0 };
            yield return new[] { 4542353, 5325325, 9867678 };

        }
    }
}
