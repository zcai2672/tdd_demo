﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TDD_Demo.BLL;
using System.Collections;

namespace TDD_Demo.Otherspace
{
    [TestFixture]
    public class Demo1_SimpleTests
    {
       
        [Test]
        [Category("add")]
        public void ShouldAddTwoNumbers()
        {
            var Cal = new Calculator();
            var result = Cal.Add(1 , 1);

            Assert.That(result, Is.EqualTo(2));
        }

        [Test]
        [Category("subtract")]
        public void ShouldSubtractTwoNumbers()
        {
            var Cal = new Calculator();
            var result = Cal.Subtract(1, 1);

            Assert.That(result, Is.EqualTo(0));
        }
    }
}
