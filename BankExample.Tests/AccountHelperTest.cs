﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankExample.Helpers;
using BankExample;
using Moq;
using BankExample.Interfaces;

namespace BankExample.Tests
{
    [TestFixture]
    public class AccountHelperTests
    {
        private Mock<IAccountDal> accountDalMock; //Test Dummy
        private AccountHelper cHelper; //Test Target
 

        [SetUp]
        public void SetUp()
        {
            this.accountDalMock =new Mock<IAccountDal>();
            
            this.cHelper = new AccountHelper(this.accountDalMock.Object);
        }


        [Test]
        public void IsSavingAccount()
        {
            this.accountDalMock.Setup(x => x.Account).Returns(Mother.GetFakeSavingAccount());
            Assert.IsTrue(this.cHelper.IsSavingAccount());
        }

        [Test]
        public void IsNotASavingAccount()
        {
            this.accountDalMock.Setup(x => x.Account).Returns(Mother.GetFakeOffsetAccount());
            Assert.IsFalse(this.cHelper.IsSavingAccount());
        }
        
        [Test]
        public void SumOfAllTransactions()
        {
            this.accountDalMock.Setup(x => x.GetTransactions()).Returns(Mother.GetFakeTransactions());
            Assert.AreEqual(this.cHelper.GetTotalTransaction(), 800);
        }
    }
}
