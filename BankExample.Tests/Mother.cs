﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankExample.Tests
{
    class Mother
    {
        public static List<Customer> GetFakeCustomers()
        {
            List<Customer> customerList = new List<Customer>();

            customerList.Add(new Customer() { Id = 1, FirstName = "Ivan", LastName = "Cai", Accounts = null });
            customerList.Add(new Customer() { Id = 2, FirstName = "Jayden", LastName = "Cai", Accounts = null });
            customerList.Add(new Customer() { Id = 3, FirstName = "Linda", LastName = "Deng", Accounts = null });

            return customerList;
        }

        internal static  List<Transaction>GetFakeTransactions()
        {
            List<Transaction> transactionList = new List<Transaction>();
            transactionList.Add(new Transaction() { Id = 1, Amount = 200 });
            transactionList.Add(new Transaction() { Id = 2, Amount = 200 });
            transactionList.Add(new Transaction() { Id = 3, Amount = 200 });
            transactionList.Add(new Transaction() { Id = 4, Amount = 200 });
            return transactionList;
        }

        internal static Account GetFakeOffsetAccount()
        {
            return new Account() { AccountType = "Offset" };
        }
        internal static Account GetFakeSavingAccount()
        {
            return new Account() { AccountType = "Saving" };
        }
    }
}
