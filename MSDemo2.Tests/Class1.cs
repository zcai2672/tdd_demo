﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSDemo2.Tests
{
    [TestFixture]
    public class Class1Tests
    {
        [Test]
        public void ShouldAddTwoNumbers()
        {
            var Cal = new Calculator();
            var result = Cal.Add(1, 1);

            Assert.That(result, Is.EqualTo(2));
        }    
    }   
}
    