﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankExample.Helpers;
using BankExample.Interfaces;
namespace BankExample
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomersDal customersDal = new CustomersDal();

            var c = new CustomersHelper(customersDal);
            Console.WriteLine("Total number of customers:" + c.TotalCustomers());

            var ci = new CustomersDal();

            Customer customer = ci.GetCustomer(1);
            Console.WriteLine("1st customer is"+customer.FirstName);

            int i=0;
            foreach (var cus in ci.GetAllCustomers())
            {
                i++;
                Console.WriteLine("customer " +i + " is: " + cus.FirstName+"\n");
            }


            IAccountDal accountDal = new AccountDal(4);
            var aHelper = new AccountHelper(accountDal);

            if (aHelper.IsSavingAccount())
            {
                Console.WriteLine( "Account:" + accountDal.Account.Id + " Is a Savings account"  );
            }

            Console.WriteLine("\n Total Transaction for this account:" + aHelper.GetTotalTransaction());

            Console.ReadKey();
        }


    }
}
