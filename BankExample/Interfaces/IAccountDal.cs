﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankExample.Interfaces
{
    public interface IAccountDal
    {
        Account Account { get; set; }
        Account GetAccount(int AccountId);
        void SetBalance(int ammount);
        List<Transaction> GetTransactions();
     
    }
}
