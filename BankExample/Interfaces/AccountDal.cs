﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankExample.Interfaces
{
    public class AccountDal:IAccountDal
    {

        public Account Account { get; set; }

        public AccountDal(int accountId)
        {
            using (var context = new BankExampleContainer())
            {
                this.Account = context.Accounts.Find(accountId);
              
            }
        }

        public void SetBalance(int amount)
        {
            using (var context = new BankExampleContainer())
            {
                Account = context.Accounts.Find(this.Account.Id);
                Account.Balance = amount;
                context.SaveChanges();
            }
        }

        public Account GetAccount(int accountId)
        {
            using (var context = new BankExampleContainer())
            {
                return context.Accounts.Find(accountId);
            }
        }

        public List<Transaction> GetTransactions()
        {
            using (var context = new BankExampleContainer())
            {
                return context.Transactions.Where(x => x.AccountId == this.Account.Id).ToList();
            }
        }
        public int GetTotalCustomers()
        {
            using (var context = new BankExampleContainer())
            {
                return context.Customers.Count();
            }
        }

    }
}
