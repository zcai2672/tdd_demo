﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankExample.Interfaces
{
    public interface ICustomersDal
    {
        Customer GetCustomer(int customerId);
        List<Customer> GetAllCustomers();
     
    }
}
