﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankExample.Interfaces
{
    public class CustomersDal:ICustomersDal
    {
        public Customer GetCustomer(int customerId)
        {
            using (var context = new BankExampleContainer())
            {
                return context.Customers.Find(customerId);
            }
        }
        public List<Customer> GetAllCustomers()
        {
            using (var context = new BankExampleContainer())
            {
                return context.Customers.AsEnumerable().ToList();
            }
        }
        public int GetTotalCustomers()
        {
            using (var context = new BankExampleContainer())
            {
                return context.Customers.Count();
            }
        }

    }
}
