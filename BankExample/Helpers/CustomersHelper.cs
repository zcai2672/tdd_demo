﻿using BankExample.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankExample.Helpers
{
    public class CustomersHelper
    {
        private readonly ICustomersDal customersDal;

        public CustomersHelper(ICustomersDal customersDal)
        {
            this.customersDal = customersDal;
        }

        public int TotalCustomers() // returning total number of customers
        {
            var allCustomers = customersDal.GetAllCustomers().Count();
            return allCustomers;
        }

    }
}
