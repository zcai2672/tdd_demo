﻿using BankExample.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankExample.Helpers
{
    public class AccountHelper
    {
        private readonly IAccountDal accountDal;

        public AccountHelper(IAccountDal accountDal)
        {
            this.accountDal = accountDal;
        }
        public bool IsSavingAccount()
        {
            if (accountDal.Account.AccountType == "Saving")
            {
                return true;
            }
            else
                return false;
        }
        public int GetTotalTransaction()
        {

            var transactions = accountDal.GetTransactions();
            var totalTransactions = transactions.Sum(x => x.Amount);
            return totalTransactions;
        }
        
    }
}
