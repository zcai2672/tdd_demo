﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TDD_Demo.BLL
{
    public class Account
    {
        public int AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Balance { get; set; }

        public Account(int accountId, string firstName, string lastName, int balance)
        {
            AccountId = accountId;
            FirstName = firstName;
            LastName = lastName;
            Balance = balance;
        }
        public int GetBalance()
        {
            // some db work to work out the balance
            return 100;
        }

    }
}