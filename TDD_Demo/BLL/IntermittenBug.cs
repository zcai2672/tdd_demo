﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace TDD_Demo.BLL
{
    public class IntermittenBug
    {
        public bool unstableFunction()
        {

            Random r = new Random();
            int binary = r.Next(1, 10);
            if(binary ==1)
                throw new ArgumentNullException();
            
            return true;
        }
    }
}