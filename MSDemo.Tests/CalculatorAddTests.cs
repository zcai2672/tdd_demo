﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Collections;

namespace MSDemo.Tests
{
    [TestFixture]
    public class CalculatorAddTest
    {
        [Test]
        public void ShouldAddTwoNumbers()
        {
            var Cal = new Calculator();
            var result = Cal.Add(1, 1);

            Assert.That(result, Is.EqualTo(2));
        }

        [TestCase(10, 10, 20)]
        [TestCase(1, 10, 11)]
        [TestCase(10, -10, 0)]
        [TestCase(10, -10, 0)] //This should fail
        public void ShouldAddTwoNumbersMultiple(int firstNum, int secondNum, int expectedResult)
        {
            var Cal = new Calculator();
            var result = Cal.Add(firstNum, secondNum);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [TestCaseSource(typeof(ExampleTestCaseSource))]
        public void ShouldAddTwoNumbersSource(int firstNum, int secondNum, int expectedResult)
        {
            var Cal = new Calculator();
            var result = Cal.Add(firstNum, secondNum);

            Assert.That(result, Is.EqualTo(expectedResult));
        }

    }

    public class ExampleTestCaseSource : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return new[] { 10, 10, 20 };
            yield return new[] { 10, -10, 0 };
            yield return new[] { 87658678, 52532453, 140191131};
          

        }
    }

}
